import sqlite3
from datetime import datetime

from pip._internal.utils.misc import tabulate

connection = sqlite3.connect("Fakultet.db")

cursor = connection.cursor()

sql_command = """
CREATE TABLE Student(
ID_Student INTEGER PRIMARY KEY,
Ime_Student VARCHAR(20),
Prezime_Student VARCHAR(30),
Datum_Rodjenja DATE,
JMBG INTEGER,
Pol CHAR,
Kontakt INTEGER,
Naziv_Fakultet VARCHAR(30),
Status_Student VARCHAR(20),
Indeks_Broj VARCHAR(20);
"""

cursor.execute(sql_command)
connection.close()



import sqlite3
connection = sqlite3.connect("Fakultet.db")

def unos_informacija(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj ):
    Datum_Rodjenja = datetime.strptime (Datum_Rodjenja, '%d/%m/%Y').date()
    qry = "INSERT INTO Student(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"
    connection.execute(qry,(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj))
    connection.commit()
    print("Dodavanje novog studenta je izvrseno!")

def izmjena_informacija(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj):
    qry = "UPDATE Student set Ime_Student = ?, Prezime_Student = ?, Datum_Rodjenja = ?, JMBG = ?, Pol = ?, Kontakt = ?, Naziv_Fakultet = ?, Status_Student = ?, Indeks_Broj = ? WHERE ID_Student = ?;"
    connection.execute(qry, (Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj, ID_Student))
    connection.commit()
    print("Izmjena podataka o studentu je izvrsena!")
    res = connection.cursor()
    qry = "UPDATE Student set Ime_Student = ?, Prezime_Student = ?, Datum_Rodjenja = ?, JMBG = ?, Pol = ?, Kontakt = ?, Naziv_Fakultet = ?, Status_Student = ?, Indeks_Broj = ? WHERE ID_Student = ?;"
    Student = (Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj, ID_Student)
    res.execute(qry, Student)
    connection.commit()
    print("Izmjena je uspjesno izvrsena!")

def brisanje_informacija (ID_Student):
    qry = "DELETE FROM Student WHERE ID_Student = ?;"
    connection.execute(qry, (ID_Student))
    connection.commit()
    print("Podaci su obrisani!")
    res = connection.cursor()
    qry = "DELETE FROM Student WHERE ID_Student = ?;"
    obrisi = (ID_Student)
    res.execute(qry, obrisi)
    connection.commit()
    print("Podaci su obrisani!")

def selektovanje_informacija():
    res = connection.cursor()
    qry = "SELECT * FROM Student"
    res.execute(qry)
    rezultat = res.fetchall()
    print(tabulate(rezultat, headers = ["ID_Student", "Ime_Student", "Prezime_Student", "Datum_Rodjenja", "JMBG", "Pol", "Kontakt", "Naziv_Fakultet", "Status_Student", "Indeks_Broj"]))
    if res.rowcount < 0:
        print("Nema podataka!")
    else:
        for podaci in rezultat:
            print(podaci)
print ("""
1. Unos novih podataka.
2. Izmjena podatak.
3.Brisanje podataka.
4.Selektovanje podataka.
""")
unos_podataka = 1
while unos_podataka == 1:
    unos = int(input("Izaberite opciju:"))
    if (unos == 1):
        print("Dodaj novog studenta")
        ID_Student = int(input("Unesi ID:"))
        Ime_Student = input("Unesi ime studenta:")
        Prezime_Student = input("Unesi prezime studenta:")
        Datum_Rodjenja = input("Unesi datum rodjenja (format: dd/mm/yyyy):")
        JMBG = int(input("Unesi jedinstveni maticni broj:"))
        Pol = input("Unesi pol:")
        Kontakt = int(input("Unesi kontakt:"))
        Naziv_Fakultet = input("Unesi naziv fakutleta:")
        Status_Student = input("Unesi status studenta:")
        Indeks_Broj = input("Unesi broj indeksa:")
        unos_informacija(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj)

    if (unos == 2):
        print("Izmjeni podatka studenta")
        ID_Student = int(input("Unesi ID:"))
        Ime_Student = input("Unesi ime studenta:")
        Prezime_Student = input("Unesi prezime studenta:")
        Datum_Rodjenja = input("Unesi datum rodjenja (format: dd/mm/yyyy):")
        JMBG = int(input("Unesi jedinstveni maticni broj:"))
        Pol = input("Unesi pol:")
        Kontakt = int(input("Unesi kontakt:"))
        Naziv_Fakultet = input("Unesi naziv fakutleta:")
        Status_Student = input("Unesi status studenta:")
        Indeks_Broj = input("Unesi broj indeksa:")
        izmjena_informacija(ID_Student, Ime_Student, Prezime_Student, Datum_Rodjenja, JMBG, Pol, Kontakt, Naziv_Fakultet, Status_Student, Indeks_Broj)

    if (unos == 3):
        print("Obrisi podatke o studentu!")
        ID_Student = input("Unesi ID da bi se podaci izbrisali:")
        brisanje_informacija(ID_Student)

    if (unos == 4):
        print("Ispisi informacije o studentima!")
        selektovanje_informacija()
    else:
        print("Greska! Pokusaj opet")


