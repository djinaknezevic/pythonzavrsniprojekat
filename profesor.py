import sqlite3

connection = sqlite3.connect("Fakultet.db")

cursor = connection.cursor()

sql_command = """
CREATE TABLE Profesor (
ID_Profesor INTEGER PRIMARY KEY,
Ime_Profesor VARCHAR (20),
Prezime_Profesor VARCHAR (30),
JMBG INTEGER,
Email VARCHAR (30),
Kontakt INTGER,
Status_Profesora VARCHAR(20),
Naziv_Fakultet VARCHAR(20),
Naziv_Predmeta VARCHAR(50);
"""
cursor.execute(sql_command)
connection.close()
from tabulate import tabulate

import sqlite3
connection = sqlite3.connect("Fakultet.db")

def unos_informacija (ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta):
    qry = "INSERT INTO Profesor (ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);"
    connection.execute(qry, (ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta))
    connection.commit()
    print("Izvrseno je dodavanje novog profesora!")

def izmjena_informacija (ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta):
    qry = "UPDATE Profesor set Ime_Profesor = ?, Prezime_Profesor = ?, JMBG = ?, Email = ?, Kontakt = ?, Status_Profesora = ?, Naziv_Fakultet = ?, Naziv_Predmeta = ? WHERE ID_Profesor = ?;"
    connection.execute(qry(ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta))
    connection.commit()
    print("Podaci o profesoru su promjenjeni!")
    res = connection.cursor()
    qry = "UPDATE Profesor set Ime_Profesor = ?, Prezime_Profesor = ?, JMBG = ?, Email = ?, Kontakt = ?, Status_Profesora = ?, Naziv_Fakultet = ?, Naziv_Predmeta = ? WHERE ID_Profesor = ?;"
    profesor = (ID_Profesor, Ime_Profesor, Prezime_Profesor, JMBG, Email, Kontakt, Status_Profesora, Naziv_Fakultet, Naziv_Predmeta)
    res.execute(qry, profesor)
    connection.commit()
    print("Uspjesno izvrsena promjena!")

def brisanje_informacija (ID_Profesor):
    qry = "DELETE FROM Profesor WHERE ID_Profesor = ?;"
    connection.execute(qry,(ID_Profesor))
    connection.commit()
    print ("Podaci su uspjesno obrisani!")
    res = connection.cursor()
    qry = "DELETE FROM Profesor WHERE ID_Profesor = ?;"
    obrisi = (ID_Profesor)
    res.execute(qry, obrisi)
    connection.commit()
    print("Podaci su obrisani!")

def selektovanje_informacija():
    res = connection.cursor()
    qry = "SELECT * FROM Profesor"
    res.execute(qry)
    rezultat = res.fetchall()
    print(tabulate(rezultat, headers=["ID_Profesor", "Ime_Profesor", "Prezime_Profesor", "JMBG", "Email", "Kontakt", "Status_Profesora", "Naziv_Fakultet", "Naziv_Predmeta"]))
    if res.rowcount < 0:
        print("Nema podataka!")
    else:
        for podaci in rezultat:
            print(podaci)

def meni_prikaz(connection):
    print("Prikazi opcije:")
    print("1. Unos novih podataka.")
    print("2. Selektovanje podataka.")
    print("3. Izmjena podataka.")
    print("4. Brisanje podataka.")

    menu = input("Izaberi opciju:")

    if menu == "1":
        unos_informacija(connection)
    elif menu == "2":
        selektovanje_informacija(connection)
    elif menu == "3":
        izmjena_informacija(connection)
    elif menu == "4":
        brisanje_informacija(connection)
    elif menu == 0:
        exit()
    else:
        print("Greska!!!")
    if __name__ == "__main__":
        while(True):
            meni_prikaz(connection)