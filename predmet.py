import sqlite3

connection = sqlite3.connect("Fakultet.db")

cursor = connection.cursor()

sql_command = """
CREATE TABLE Predmet(
ID_Predmet INTEGER PRIMARY KEY,
Naziv_Predmet VARCHAR(50),
Fond_Casova INTEGER,
Semestar VARCHAR(20,
ID_Profesor INTEGER)
"""

cursor.execute(sql_command)
connection.close()
from tabulate import tabulate

import sqlite3
connection = sqlite3.connect("Fakultet.db")

def unos_informacija(ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor):
    qry = "INSERT INTO Predmet (ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor) VALUES (?,?,?,?,?); "
    connection.execute(qry,(ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor))
    connection.commit()
    print("Dodan je novi predmet!")

def izmjena_informacija(ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor):
    qry = "UPDATE Predmet set  Naziv_Predmet = ?, Fond_Casova = ?, Semestar = ?, ID_Profesor = ? WHERE ID_Predmet = ?;"
    connection.execute(qry,(ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor))
    connection.commit()
    print("Podaci o predmetu su izmjenjeni!")
    res = connection.cursor()
    qry = "UPDATE Predmet set  Naziv_Predmet = ?, Fond_Casova = ?, Semestar = ?, ID_Profesor = ? WHERE ID_Predmet = ?;"
    predmet = (ID_Predmet, Naziv_Predmet, Fond_Casova, Semestar, ID_Profesor)
    res.execute(qry, predmet)
    connection.commit()
    print("Uspjesno izvrsena izmjena!")

def brisanje_informacija (ID_Predmet):
    qry = "DELETE FROM Predmet WHERE ID_Predmet = ?;"
    connection.execute(qry, (ID_Predmet))
    connection.commit()
    print("Podacu su obrisani!")
    res = connection.cursor()
    qry = "DELETE FROM Predmet WHERE ID_Predmet = ?;"
    izbrisi = (ID_Predmet)
    res.execute(qry, izbrisi)
    connection.commit()
    print("Podaci su obrisani!")

def selektovanje_informacija ():
    res = connection.cursor()
    qry = "SELECT * FROM Predmet"
    res.execute(qry)
    rezultat = res.fetchall()
    print(tabulate(rezultat, headers=["ID_Predmet", "Naziv_Predmet", "Fond_Casova", "Semestar", "ID_Profesor"]))
    if res.rowcount < 0:
        print("Nema podataka!")
    else:
        for podaci in rezultat:
            print(podaci)

def meni_prikaz(connection):
    print("Prikazi opcije:")
    print("1. Unos novih podataka.")
    print("2. Selektovanje podataka.")
    print("3. Izmjena podataka.")
    print("4. Brisanje podataka.")

    menu = input("Izaberi opciju:")

    if menu == "1":
        unos_informacija(connection)
    elif menu == "2":
        selektovanje_informacija(connection)
    elif menu == "3":
        izmjena_informacija(connection)
    elif menu == "4":
        brisanje_informacija(connection)
    elif menu == 0:
        exit()
    else:
        print("Greska!!!")
    if __name__ == "__main__":
        while(True):
            meni_prikaz(connection)