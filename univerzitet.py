import sqlite3

connection = sqlite3.connect("Fakultet.db")

cursor = connection.cursor()

sql_command = """
CREATE TABLE Univerzitet (
ID_Fakultet INTEGER PRIMARY KEY,
Naziv_Fakultet VARCHAR(30),
Broj_Studenata INTEGER,
Grad VARCHAR(30);
"""

cursor.execute(sql_command)
connection.close()
from tabulate import tabulate

import sqlite3
connection = sqlite3.connect("Fakultet.db")

def unos_informacija (ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad):
    qry = "INSERT INTO Univerzitet (ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad) VALUES (?,?,?,?);"
    connection.execute(qry(ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad))
    connection.commit()
    print("Dodan je novi naziv fakulteta na univerzitetu.")

def izmjena_informacija (ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad):
    qry = "UPDATE Univerzitet SET Naziv_Fakultet = ?, Broj_Studenta = ?, Grad = ? WHERE ID_Fakultet = ?;"
    connection.execute(qry,(ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad))
    connection.commit()
    print("Podaci o fakultetu su izmijenjeni.")
    res = connection.cursor()
    qry = "UPDATE Univerzitet SET Naziv_Fakultet = ?, Broj_Studenta = ?, Grad = ? WHERE ID_Fakultet = ?;"
    fakultet = (ID_Fakultet, Naziv_Fakultet, Broj_Studenta, Grad)
    res.execute(qry, fakultet)
    connection.commit()
    print("Izmjena je uspjesno izvrsena!")

def brisanje_informacija (ID_Fakultet):
    qry = "DELETE FROM Univerzitet WHERE ID_Fakultet = ?;"
    connection.execute(qry, (ID_Fakultet))
    connection.commit()
    print("Podaci su izbrisani!")
    res = connection.cursor()
    qry = "DELETE FFROM Univerzitet WHERE ID_Fakultet = ?;"
    obrisi = (ID_Fakultet)
    res.execute(qry, obrisi)
    connection.commit()
    print("Podaci su obrisani!")

def selektovanje_informacija ():
    res = connection.cursor()
    qry = "SELECT * FROM Univerzitet"
    res.execute(qry)
    rezultat = res.fetchall()
    print(tabulate(rezultat, headers=["ID_Fakultet", "Naziv_Fakultet", "Broj_Studenta", "Grad"]))
    if res.rowcount < 0:
        print("Nema podataka!")
    else:
        for podaci in rezultat:
            print(podaci)

def meni_prikaz(connection):
    print("Prikazi opcije:")
    print("1. Unos novih podataka.")
    print("2. Selektovanje podataka.")
    print("3. Izmjena podataka.")
    print("4. Brisanje podataka.")

    menu = input("Izaberi opciju:")

    if menu == "1":
        unos_informacija(connection)
    elif menu == "2":
        selektovanje_informacija(connection)
    elif menu == "3":
        izmjena_informacija(connection)
    elif menu == "4":
        brisanje_informacija(connection)
    elif menu == 0:
        exit()
    else:
        print("Greska!!!")
    if __name__ == "__main__":
        while(True):
            meni_prikaz(connection)